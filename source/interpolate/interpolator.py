import numpy as np
from scipy.interpolate import griddata
from sklearn.preprocessing import scale


class Interpolator:
    NUM_CHANS = 3
    NUM_CORNERS = 4

    def __init__(self, electrode_locations: np.ndarray, num_grid_points: int) -> None:
        self.grid_x, self.grid_y = np.mgrid[
            min(electrode_locations[:, 0]) : max(
                electrode_locations[:, 0]
            ) : num_grid_points  # type: ignore
            * 1j,
            min(electrode_locations[:, 1]) : max(
                electrode_locations[:, 1]
            ) : num_grid_points  # type: ignore
            * 1j,
        ]
        self.map_buffer = np.zeros(
            (
                Interpolator.NUM_CHANS,
                num_grid_points,
                num_grid_points,
            ),
            dtype=np.float32,
        )

        # Add interpolated electrodes to image corners
        min_x, min_y = np.min(electrode_locations, axis=0)
        max_x, max_y = np.max(electrode_locations, axis=0)
        self.virtual_locations = np.append(
            electrode_locations,
            np.array([[min_x, min_y], [min_x, max_y], [max_x, min_y], [max_x, max_y]]),
            axis=0,
        )

    def interpolate(self, image_data: np.ndarray, normalise=False) -> np.ndarray:
        # Add empty spaces for virtual locations, which correspond to image corners
        interpolated_image = np.array(
            [
                np.append(image_data[n], np.zeros(Interpolator.NUM_CORNERS))
                for n in range(Interpolator.NUM_CHANS)
            ]
        )

        # Note, self.grid_x and self.grid_y have been swapped to try remove nans from image.
        # This results in more nans, but limits them to top two rows and bottom row, which
        # at least looks ok. Eventually I'll need to find out why they're generated and get
        # rid of them altogether
        for c in range(Interpolator.NUM_CHANS):
            self.map_buffer[c, :] = griddata(
                self.virtual_locations,
                interpolated_image[c, :],
                (self.grid_y, self.grid_x),
                method="cubic",
            )

            if normalise is True:
                self.map_buffer[c][~np.isnan(self.map_buffer[c])] = scale(
                    self.map_buffer[c][~np.isnan(self.map_buffer[c])]
                )
        return self.map_buffer
