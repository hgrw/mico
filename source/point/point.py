import math as m

import mne
import numpy as np


def azim_proj(pos):
    """
    Computes the Azimuthal Equidistant Projection of input point in 3D Cartesian Coordinates.
    Imagine a plane being placed against (tangent to) a globe. If
    a light source inside the globe projects the graticule onto
    the plane the result would be a planar, or azimuthal, map
    projection.
    :param pos: position in 3D Cartesian coordinates
    :return: projected coordinates using Azimuthal Equidistant Projection
    """
    [_, elev, az] = cart2sph(pos[0], pos[1], pos[2])
    return pol2cart(az, m.pi / 2 - elev)


def cart2sph(x, y, z):
    """
    Transform Cartesian coordinates to spherical
    :param x: X coordinate
    :param y: Y coordinate
    :param z: Z coordinate
    :return: radius, elevation, azimuth
    """
    x2_y2 = x**2 + y**2
    r = m.sqrt(x2_y2 + z**2)  # r
    elev = m.atan2(z, m.sqrt(x2_y2))  # Elevation
    az = m.atan2(y, x)  # Azimuth
    return r, elev, az


def pol2cart(theta, rho):
    """
    Transform polar coordinates to Cartesian
    :param theta: angle value
    :param rho: radius value
    :return: X, Y
    """
    return rho * m.cos(theta), rho * m.sin(theta)


def get_positions(electrode_list: list) -> np.ndarray:
    """
    Transform polar coordinates to Cartesian
    :param electrode_list: List of electrode identifiers (strings)
    :return: [[X, Y], ...], list of azimuth equidistant projected points in 2d
    """
    standard_1020_electrodes = mne.channels.make_standard_montage(
        "standard_1020"
    ).get_positions()
    locs_3d = [
        standard_1020_electrodes["ch_pos"][electrode] for electrode in electrode_list
    ]
    return np.array([azim_proj(position) for position in locs_3d])
