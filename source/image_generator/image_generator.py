import pathlib
from datetime import datetime
from enum import IntEnum

import numpy as np
from cv2 import INTER_CUBIC, VideoWriter, VideoWriter_fourcc, imshow, resize, waitKey

from source.headset.headset import load_config
from source.interpolate.interpolator import Interpolator


class Generator:
    FRAME_RATE_FPS = 50
    TICKS_MS = 1000
    UINT8_SCALE = 255

    class Colours(IntEnum):
        BLUE = 0
        GREEN = 1
        RED = 2

    class Defaults(IntEnum):
        INTERMEDIATE_RESOLUTION = 50
        VISUALISER_RESOLUTION = 500

    def __init__(self, electrode_locations: list) -> None:
        config = load_config(
            str(pathlib.Path(__file__).parent.resolve())
            + "/../../config/application.yaml"
        )
        self._resolution = config.get(
            "visualiser_resolution", Generator.Defaults.VISUALISER_RESOLUTION
        )
        self._save_data = config["train_data"].get("save", False)
        self._save_video = config["video"].get("save", False)
        self._interpolator = Interpolator(
            np.array(electrode_locations),
            int(
                config.get(
                    "intermediate_resolution",
                    Generator.Defaults.INTERMEDIATE_RESOLUTION,
                )
            ),
        )
        self._wait_duration_ms = int(Generator.TICKS_MS / Generator.FRAME_RATE_FPS)
        self._save_dir = str(pathlib.Path(__file__).parent.resolve()) + "/../../data/"
        if self._save_video:
            self._video = VideoWriter()
            self._video.open(
                self._save_dir + str(datetime.timestamp(datetime.now())) + ".avi",
                VideoWriter_fourcc(*config["video"].get("codec", "MJPG")),
                25,
                (self._resolution, self._resolution),
                True,
            )

    def __del__(self):
        if self._save_video:
            print("Saving video to " + self._save_dir)
            self._video.release()

    def generate(self, sensor_data: np.ndarray, plot=False) -> None:
        data = self._interpolator.interpolate(sensor_data, normalise=True)
        frame = np.dstack(
            (
                data[Generator.Colours.BLUE, :, :],
                data[Generator.Colours.GREEN, :, :],
                data[Generator.Colours.RED, :, :],
            )
        ) - np.nanmin(data)
        if plot:
            image = resize(
                ((frame / np.nanmax(frame)) * 255.0).astype(np.uint8),
                (self._resolution, self._resolution),
                interpolation=INTER_CUBIC,
            )

            imshow("MICO Image Generator", image)
            if self._save_video:
                self._video.write(image)
            waitKey(self._wait_duration_ms)
