import pathlib
import sys
from enum import IntEnum

import numpy as np
import yaml
from pylsl import StreamInlet, resolve_stream
from yaml.parser import ParserError
from yaml.scanner import ScannerError

from source.point.point import get_positions


def load_config(config_file_path: str) -> dict:
    """
    Load application configuration. Checks config file for validity
    :param config_file_path: yaml file with application config
    :return: param dict if valid, otherwise exit(0)
    """

    valid_bands = ["delta", "beta", "alpha", "theta", "gamma"]
    try:
        with open(config_file_path, "r", encoding="utf8") as cfg:
            try:
                config = yaml.safe_load(cfg)
                for band in config["rgb_bands"]:
                    if config["rgb_bands"][band] not in valid_bands:
                        print("Invalid config file")
                        exit(0)
                return config
            except (ScannerError, ParserError):
                print("Config file broken. Yaml scanner/parser error")
                exit(0)
    except FileNotFoundError:
        print("Config file not found")
        exit(0)


class Headset:
    NUM_ELECTRODES = 16
    STREAM_INDEX = 0
    SAMPLE_TIMEOUT_S = 1

    class Bands(IntEnum):
        DELTA = 0
        THETA = 1
        ALPHA = 2
        BETA = 3
        GAMMA = 4
        QUANTITY = 5

    def __init__(self) -> None:
        config = load_config(
            str(pathlib.Path(__file__).parent.resolve())
            + "/../../config/application.yaml"
        )

        if "pytest" not in sys.modules:  # Don't resolve stream if unit testing
            stream = resolve_stream("name", "band_power")[Headset.STREAM_INDEX]
            self._inlet = StreamInlet(stream)
        else:
            self._inlet = None

        self._sample_buffer = np.nan * np.ones(
            (Headset.NUM_ELECTRODES, Headset.Bands.QUANTITY), dtype=np.float32
        )
        self._band_map = {
            "delta": Headset.Bands.DELTA,
            "theta": Headset.Bands.THETA,
            "alpha": Headset.Bands.ALPHA,
            "beta": Headset.Bands.BETA,
            "gamma": Headset.Bands.GAMMA,
        }
        self._colours = [
            self._band_map[config["rgb_bands"]["blue"]],
            self._band_map[config["rgb_bands"]["green"]],
            self._band_map[config["rgb_bands"]["red"]],
        ]
        self.electrode_locations = get_positions(config["electrodes"])

    def pull_sample(self) -> np.ndarray:
        self._inlet.pull_chunk(
            timeout=Headset.SAMPLE_TIMEOUT_S,
            max_samples=Headset.NUM_ELECTRODES,
            dest_obj=self._sample_buffer,
        )
        return np.array([self._sample_buffer[:, n] for n in self._colours])
