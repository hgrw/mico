from source.headset.headset import Headset
from source.image_generator.image_generator import Generator

if __name__ == "__main__":

    headset = Headset()
    generator = Generator(headset.electrode_locations)

    while True:
        # Get sample
        sample = headset.pull_sample()

        # Convert sample into image
        generator.generate(sample, plot=True)
