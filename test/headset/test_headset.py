import pathlib

import pytest

from source.headset.headset import Headset, load_config


# Check that wrong config file produces exit(0)
def test_load_config():
    with pytest.raises(SystemExit) as result:
        load_config("None")
    assert result.type == SystemExit
    assert result.value.code == 0
    with pytest.raises(SystemExit) as result:
        load_config("headset/faulty_config.yaml")
    assert result.type == SystemExit
    assert result.value.code == 0


# Check that config loader checks that colours are valid
def test_load_config_bad_colours():
    with pytest.raises(SystemExit) as result:
        load_config("headset/incorrect_config.yaml")
    assert result.type == SystemExit
    assert result.value.code == 0


# Check that rgb bands are mapped according to this spec
def test_parse_config_rgb_bands():
    config = load_config(
        str(pathlib.Path(__file__).parent.resolve()) + "/../../config/application.yaml"
    )
    delta = 0
    theta = 1
    alpha = 2
    beta = 3
    gamma = 4
    blue = config["rgb_bands"]["blue"]
    green = config["rgb_bands"]["green"]
    red = config["rgb_bands"]["red"]
    assert True
    h = Headset()
    for band in [
        ["delta", delta],
        ["beta", beta],
        ["alpha", alpha],
        ["theta", theta],
        ["gamma", gamma],
    ]:
        if band[0] == blue:
            assert int(h._colours[0]) == band[1]
        if band[0] == green:
            assert int(h._colours[1]) == band[1]
        if band[0] == red:
            assert int(h._colours[2]) == band[1]
